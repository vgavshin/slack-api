var request = require('../request/request.js');
var debug = require('debug')('delete');

module.exports = (options, callback) => { 
  request('files.delete', options, (err, data) => {
    if (err) {
      debug(`Problem with request: ${err.message}`)
      callback(err, null)
      
      return;
    }
    debug(`Request data: ${data}`)
    callback(null, data)
  })
}
