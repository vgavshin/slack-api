var request = require('../request/request.js');
var debug = require('debug')('list');

module.exports = (options, callback) => { 
  request('files.list', options, (err, data) => {
    if (err) {
      debug(`Problem with request: ${err.message}`)
      callback(err, null)
      
      return;
    }
    debug(`Request data: ${data}`)
    callback(null, data)
  })
}
