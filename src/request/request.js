var https = require('https');
const querystring = require('query-string');
var debug = require('debug')('request');

const DEFAULT_BODY = {
  token: process.env.SLACK_TOKEN
}

module.exports = (method, props, callback) => {
  var body = querystring.stringify(Object.assign({}, DEFAULT_BODY, props));

  const options = {
    hostname: 'slack.com',
    port: 443,
    path: `/api/${method}`,
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Length': Buffer.byteLength(body)
    }
  };

  var req = https.request(options, (res) => {
    debug(`STATUS: ${res.statusCode}`);
    debug(`HEADERS: ${JSON.stringify(res.headers)}`);
    var data = '';

    res.setEncoding('utf8');
    res.on('data', (chunk) => {
      debug(`CHUNK: ${data}`);
      data += chunk;
    });
    res.on('end', () => {
      if (isBadStatus(res.statusCode, callback)) return;
      var parsed_data = JSON.parse(data);

      if (isBadResponse(parsed_data, callback)) return;
      debug('No more data in response.');
      callback(null, parsed_data)
    });
  });

  req.on('error', (err) => {
    debug(`Problem with request: ${err.message}`);
    callback(err, null)
  });

  req.write(body);
  req.end();
}


var isBadStatus = (code, callback) => {
  if (code !== 200) {
    debug('Response status not 200.');
    callback({ 
      message: 'Response status not 200',
      status: code
    }, null);

    return true;
  }
}

var isBadResponse = (data, callback) => {
  if (!data.ok) {
    debug(`Response failed ${data.error}`);
    callback({ 
      message: data.error
    }, null);

    return true;
  }
}
