require('dotenv').config();
const expect = require('chai').expect;
var request = require('../api/list.js')

describe('Test List:', ()  => {
  it('getting "ok" respone with valid empty props', (done) => {
    request({}, (err, data) => {
      expect(err).to.eql(null);
      expect(data).not.to.eql(null);
      expect(data.ok).to.eql(true);
      done();
    });
  });

  // Return files array in data.files
  // it('Return files array in data.files', (done) => {
  //   request({}, (err, data) => {
  //     expect(err).to.eql(null);
  //     expect(data).not.to.eql(null);
  //     expect(data.ok).to.eql(true);
  //     done();
  //   });
  // });

  it('getting "ok" respone with props', (done) => {
    request({foo: 'bar'}, (err, data) => {
      expect(err).to.eql(null);
      expect(data).not.to.eql(null);
      expect(data.ok).to.eql(true);
      done();
    });
  });

  it('getting "error" respone with empty token', (done) => {
    request({token: 'bar'}, (err, data) => {
      expect(err).not.to.eql(null);
      expect(data).to.eql(null);
      done();
    });
  });
});
