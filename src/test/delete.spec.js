require('dotenv').config();
const expect = require('chai').expect;
var request = require('../api/delete.js')

describe('Test List:', ()  => {
  // it('getting "ok" respone with props', (done) => {
  //   request({foo: 'bar'}, (err, data) => {
  //     expect(err).to.eql(null);
  //     expect(data).not.to.eql(null);
  //     expect(data.ok).to.eql(true);
  //     done();
  //   });
  // });

  it('getting "error" respone with empty props', (done) => {
    request({}, (err, data) => {
      expect(err).not.to.eql(null);
      expect(data).to.eql(null);
      done();
    });
  });

  it('getting "error" respone with empty token', (done) => {
    request({token: 'bar'}, (err, data) => {
      expect(err).not.to.eql(null);
      expect(data).to.eql(null);
      done();
    });
  });
});
