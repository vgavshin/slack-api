require('dotenv').config();
const expect = require('chai').expect;
var request = require('../request/request.js')

describe('Test Request:', ()  => {
  it('getting "ok" respone with valid method and empty props', (done) => {
    request('api.test', {}, (err, data) => {
      expect(err).to.eql(null);
      expect(data).not.to.eql(null);
      expect(data.ok).to.eql(true);
      done();
    });
  });

  it('getting "ok" respone with valid method and props', (done) => {
    request('api.test', {foo: 'bar'}, (err, data) => {
      expect(err).to.eql(null);
      expect(data).not.to.eql(null);
      expect(data.ok).to.eql(true);
      expect(data.args.foo).to.eql('bar');
      done();
    });
  });

  it('getting errors with invalid method', (done) => {
    request('api.test/some_string', {}, (err, data) => {
      expect(err).not.to.eql(null);
      expect(data).to.eql(null);
      done();
    });
  });

  it('getting errors respone with empty token', (done) => {
    request('api.test', {token: 'bar'}, (err, data) => {
      expect(err).not.to.eql(null);
      expect(data).to.eql(null);
      done();
    });
  });
});
