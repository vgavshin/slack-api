require('dotenv').config();
var list = require('./api/list.js');
var delete_file = require('./api/delete.js');

const ts_to = Math.round(Date.now()/1000 - 3600 * 24 * 90);

console.log(ts_to);
list({ts_to: ts_to}, (err, data) => {
  data.files.map( element => {
    delete_file({file: element.id}, (err, data) => {
      console.log(data, `Delete file ${element.id}`);
    })
  });
})
